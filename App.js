import React,{useEffect} from 'react'
import {LogBox} from 'react-native'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/navigation/Navigator'
import SplashScreen from 'react-native-splash-screen';


LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 500);
    }, [])
    

    return (
        <NavigationContainer>
            <Navigator />
        </NavigationContainer>
    )
}

export default App