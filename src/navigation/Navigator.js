import { Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import ArticleScreen from '../screens/ArticleScreen';
import DetailsScreen from '../screens/DetailsScreen';


const Stack = createStackNavigator();

const App = ({navigation}) => {

    useEffect(() => {

    }, [])

    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Details"
                component={DetailsScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Article"
                component={ArticleScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    )
}

export default App
