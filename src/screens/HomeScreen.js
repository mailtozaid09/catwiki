import React,{useState, useEffect} from 'react'
import { Text, View, StyleSheet, ScrollView, Keyboard, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import Colors from '../style/Colors';
import Header from '../components/logo/Logo';
import SearchBar from '../components/search/SearchBar';
import CatBreeds from '../components/breeds/CatBreeds';
import Footer from '../components/footer/Footer';
import CatDescription from '../components/description/CatDescription';
import BarLine from '../components/bar/BarLine';
import { getCatBreeds } from '../api/api';
import Loader from '../components/loader/Loader';

const {width} = Dimensions.get('window');

const HomeScreen = () => {

    const navigation = useNavigation();

    const [breeds, setBreeds] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        
        getCatBreeds()
        .then((resp) => {
            //console.log(resp);
            setBreeds(resp)

            setLoader(false)
        })
        .catch((error) => {            
            console.log(error);
        })
        
    }, [])
    
    
    return (
        <View style={styles.container} >

            {loader
            ?
                <Loader />
            :
            <View>
                 <Header logoImg={require('../assets/white-logo.png')} />
            
            <ScrollView>
                <SearchBar 
                    title="Get to know more about your cat breed"
                    logoImg={require('../assets/logo-title.png')} 
                    data={breeds}
                />

                <CatBreeds 
                    title="Most Searched Breeds"
                    subTitle="66+ Breeds For you to discover"
                    data={breeds}
                />

                <View style={styles.barline} >
                    <BarLine/>
                </View>


                <CatDescription
                    readMore={() => navigation.navigate('Article')}
                    title="Why should you have a cat?"
                    subTitle="Having a cat around you can actually trigger the release of calming chemicals in your body which lower your stress and anxiety leves"
                />


                <Footer
                    logoImg={require('../assets/black-logo.png')}
                />
            </ScrollView>
            </View>
            }
            
           
           
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    barline: {
        padding: 20
    }
})

export default HomeScreen