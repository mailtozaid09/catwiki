import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import Colors from '../style/Colors';
import Header from '../components/logo/Logo';

import { Rating, AirbnbRating } from 'react-native-ratings';
import Footer from '../components/footer/Footer';
import Loader from '../components/loader/Loader';
import { getReferenceImg } from '../api/api';


const {width} = Dimensions.get('window');

const catImages = [
    {
        image: require('../assets/cat/cat1.png')
    },
    {
        image: require('../assets/cat/cat2.png')
    },
    {
        image: require('../assets/cat/cat3.png')
    },
     {
        image: require('../assets/cat/cat4.png')
    },
    {
        image: require('../assets/cat/cat5.png')
    },
    {
        image: require('../assets/cat/cat6.png')
    },
    {
        image: require('../assets/cat/cat7.png')
    },
     {
        image: require('../assets/cat/cat8.png')
    },
]

const DetailsScreen = (props) => {

    const navigation = useNavigation();

    const [details, setDetails] = useState({});
    const [imgUrl, setImgUrl] = useState('');

    const [loader, setLoader] = useState(true);

    useEffect(() => {
        console.log("DetailsScreen---- " );

        var details = props.route.params.details
        var refId = props.route.params.referenceId

        getReferenceImg(refId)
        .then((resp) => {
            console.log(resp.url)
            setDetails(details)
            setImgUrl(resp.url)
            setTimeout(() => {
                setLoader(false)
            }, 500);
        })
        .catch((err) => {
            console.log(err)
        })
    }, [])
    

    return (
        <View style={styles.mainContainer} >
            {loader
            ?
                <Loader />
            :
            <View>
                <Header backIcon logoImg={require('../assets/white-logo.png')} />
                <ScrollView>

                    <View style={styles.container} >
                        <Text style={styles.title} >{details.name}</Text>
                        <Text style={styles.description} >{details.description}</Text>
                    

                        <View style={styles.imageContainer} >
                            <View style={styles.bar} >
                                <Text></Text>
                            </View>
                            <Image
                                style={styles.image}
                                source={{uri: imgUrl}}
                            />
                        </View>

                    

                        <View style={styles.headingContainer} >
                            <Text style={styles.heading} >Temperament : </Text>
                            <View style={{ flex:1,}} >
                                <Text style={styles.headingValue} >{details.temperament}</Text>
                            </View>
                        </View>


                        <View style={styles.headingContainer} >
                            <Text style={styles.heading} >Origin : </Text>
                            <View style={{ flex:1,}} >
                                <Text style={styles.headingValue} >{details.origin}</Text>
                            </View>
                        </View>

                        <View style={styles.headingContainer} >
                            <Text style={styles.heading} >Life Span : </Text>
                            <View style={{ flex:1,}} >
                                <Text style={styles.headingValue} >{details.life_span}</Text>
                            </View>
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Adaptability : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.adaptability}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Affection level : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.affection_level}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Child Friendly : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.child_friendly}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Grooming : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.grooming}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Intelligence : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.intelligence}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Health issues : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.health_issues}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Social needs : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.social_needs}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View style={styles.levelContainer} >
                            <Text style={styles.heading} >Stranger friendly : </Text>
                            <AirbnbRating 
                                size={30}
                                defaultRating={details.stranger_friendly}
                                showRating={false}
                                selectedColor={'#544439'}
                                unSelectedColor={'#E0E0E0'}
                                starImage={require('../assets/bar.png')}
                                ratingContainerStyle={{height: 25, width: 150}}
                            />
                        </View>

                        <View>
                            <Text style={[styles.title, {marginTop: 20}]} >Other photos</Text>
                            <FlatList
                                data={catImages}
                                contentContainerStyle={styles.flatContainer}
                                renderItem={({item}) => (
                                    <View style={styles.catImage} >
                                    <Image
                                            source={item.image}
                                            style={styles.imageStyle}
                                        />
                                    </View>
                                )}
                            />
                        </View>


                    

                    </View>
                    
                    <View style={{height: 200}} >
                        <Footer
                            logoImg={require('../assets/black-logo.png')}
                        />
                    </View>
                    
                </ScrollView>
            </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    container: {
        padding: 20,
        paddingTop: 0,
    },
    title: {
        fontSize: 32,
        lineHeight: 38,
        marginBottom: 10,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-SemiBold'
    },
    description: {
        fontSize: 18,
        lineHeight: 24,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    headingContainer: {
        flexDirection: 'row',
        width: width-40
    },
    heading: {
        fontSize: 15,
        lineHeight: 22,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    },
    headingValue: {
        fontSize: 15,
        lineHeight: 22,
        marginBottom: 10,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    levelContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bar: {
        height: width-120, 
        width: 30, 
        backgroundColor: Colors.YELLOW, 
        position: 'absolute', 
        top: 20,
        borderRadius: 20,
    },
    image: {
        height: width-80,
        width: width-80,
        marginLeft: 10,
        borderRadius: 20
    },
    imageContainer: {
        margin: 10,
        marginBottom: 30
    },
    flatContainer: {
        flexWrap: 'wrap', 
        justifyContent: 'space-between', 
        flexDirection: 'row',
    },
    catImage: {
        marginTop: 20, 
        width: width/2-30, 
    },
    imageStyle: {
        height:  width/2-30, 
        width:  width/2-30,
        borderRadius: 10,
    },
})
export default DetailsScreen

