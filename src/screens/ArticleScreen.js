import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import Colors from '../style/Colors';
import Header from '../components/logo/Logo';
import Footer from '../components/footer/Footer';

const {width} = Dimensions.get('window');

const reasons = [
    {
        id: 1,
        title: 'Cats can bathe themselves',
        subTitle: 'Cats are clean pretty much 100 percent of the time. That means you never have to take the time out of your day to perform the somewhat painstaking task of washing and grooming your cat.',
    },
    {
        id: 2,
        title: 'Cats will keep your house and yard rodent-free',
        subTitle: 'If you are not a fan of rats, chipmunk, voles or mice in your home, owning a cat will take care of that right away. Your cat may even bring you its prize to make you proud!',
    },
    {
        id: 3,
        title: 'Cats are low-maintenance and independent',
        subTitle: 'If you think you do not have the time or energy to own a pet, then a cat could be perfect for you. Taking care of a cat requires less responsibility than some other animals. Those who have full-time jobs can rest easy, knowing that their kitty can take care of itself for the most part. And when you do have extra time, cuddling up with your cat will feel better than ever.',
    },
    {
        id: 4,
        title: 'Cats are an eco-friendly pet choice',
        subTitle: 'Living a “green” lifestyle can be difficult, but a cat is a great choice for potential pet owners looking to stay eco-friendly. Studies show that the lifetime resources needed to feed and care for a cat have a smaller carbon footprint than for other animals. Plus, most cats prefer fish to beef or corn, which is better for the environment. You can feel good about owning your kitty.',
    },
    {
        id: 5,
        title: 'Cats can help reduce stress',
        subTitle: 'We all get stressed out, and people have many different ways of relieving their stress. Cat owners can reduce tensions by just stroking their furry friend’s head. Petting a cat releases endorphins into the brain, which makes you happier. Also, cats have the softest fur!',
    },
]

const catImages = [
    {
        image: require('../assets/cat/cat2.png')
    },
    {
        image: require('../assets/cat/cat3.png')
    },
    {
        image: require('../assets/cat/cat4.png')
    },
     {
        image: require('../assets/cat/cat1.png')
    },
    {
        image: require('../assets/cat/cat5.png')
    },
    {
        image: require('../assets/cat/cat6.png')
    },
    {
        image: require('../assets/cat/cat7.png')
    },
     {
        image: require('../assets/cat/cat8.png')
    },
]

const ArticleScreen = () => {
    return (
        <View style={styles.mainContainer} >
            <Header backIcon logoImg={require('../assets/white-logo.png')} />
            <ScrollView>

                <View style={styles.container} >
                    <Text style={styles.titleStyle} >Why should you have a cat?</Text> 
                    <Text style={styles.subTitle} >Having a cat around you can actually trigger the release of calming chemicals in your body which lower your stress and anxiety leves</Text> 
                    
                
                    <View>
                        <Text style={[styles.title, {marginTop: 10}]} >Photos</Text>
                        <FlatList
                            data={catImages}
                            nestedScrollEnabled
                            horizontal
                            contentContainerStyle={styles.flatContainer}
                            renderItem={({item}) => (
                                <View style={styles.catImage} >
                                    <Image
                                        source={item.image}
                                        style={styles.imageStyle}
                                    />
                                </View>
                            )}
                        />
                    </View>

                    <Text style={styles.subTitle} >Cats are one of the best pets you can get. If you are hesitant to take on the responsibility of owning a pet, you might want to look again at the benefits of having a cat. They are sweet, quiet and independent, and hearing a cat’s purr can melt your heart. Here are the top five reasons you should own a cat.</Text>
                                
                    <View>
                        <Text style={[styles.title, {marginTop: 10}]} >Reasons</Text>
                        <FlatList
                            data={reasons}
                            nestedScrollEnabled
                            contentContainerStyle={styles.reasonContainer}
                            renderItem={({item}) => (
                                <View  >
                                    <Text style={styles.reasonTitle} >{item.id}. {item.title}</Text>
                                    <Text style={styles.reasonSubTitle} >{item.subTitle}</Text>
                                </View>
                            )}
                        />
                    </View>
                
                </View>

                <View style={{height: 130}} >
                    <Footer
                        logoImg={require('../assets/black-logo.png')}
                    />
                </View>
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    container: {
        padding: 20,
        paddingTop: 10,
    },
    title: {
        fontSize: 28,
        lineHeight: 32,
        marginBottom: 10,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-SemiBold'
    },
    titleStyle: {
        fontSize: 28,
        lineHeight: 30,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    },
    subTitle: {
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    image: {
        height: width-80,
        width: width-80,
        marginLeft: 10,
        borderRadius: 20
    },
    imageContainer: {
        margin: 10,
        marginBottom: 30
    },
    flatContainer: {
        flexDirection: 'row',
        marginBottom: 30,
    },
    reasonContainer: {
        marginBottom: 10,
    },
    catImage: {
        marginTop: 20, 
        //width: width/2-30, 
    },
    imageStyle: {
        marginRight: 20,
        height:  width/2, 
        width:  width/2,
        borderRadius: 10,
    },
    reasonTitle: {
        fontSize: 18,
        lineHeight: 24,
        marginBottom: 5,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    },
    reasonSubTitle: {
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
});


export default ArticleScreen