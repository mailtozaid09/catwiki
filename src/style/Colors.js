const Colors = {
    RED: '#e70121',
    GREEN: '#3ecf8e',
    BLACK: '#050709',
    WHITE: '#FFFFFF',
    GRAY: '#A9A9A9',
    YELLOW: '#DEC68B',
    BLUE: '#0341c4',
    LIGHT_BLACK: '#291507',
    LIGHT_YELLOW: '#f2ba5f',
    CREAM: '#E3E1DC',
};

export default Colors;
