import React, {useState, useEffect} from 'react'
import { Text, View, ActivityIndicator, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import Colors from '../../style/Colors'



const Loader = () => {
    return (
        <View style={styles.loader} >
            <ActivityIndicator size='large' color={Colors.BLACK} />
        </View>
    )
}

const styles = StyleSheet.create({
    loader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
export default Loader