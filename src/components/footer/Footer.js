import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, ImageBackground, } from 'react-native';

import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Footer = (props) => {

    useEffect(() => {

    }, [])
    
    const {logoImg} = props;

    return (
        <View style={styles.mainContainer} >

            <Image
                source={logoImg}
                style={styles.logo}
            />

            <View style={styles.container} >
                <Image
                    source={require('../../assets/copyrights.png')}
                    style={styles.copyrights}
                />
                <View >
                    <Text style={styles.titleStyle} >created by <Text style={styles.username} >username</Text>  - devChallenge.io 2021</Text> 
                </View>

              
            </View>
        </View>
    )
}

export default Footer;


const styles = StyleSheet.create({
    mainContainer: {
        width: width-40,
        margin: 20,
        marginTop: 10,
        marginBottom: 0,
        height: 180,
        padding: 20,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: Colors.BLACK,
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logo: {
        height: 30, 
        width: 120,
    },
    titleStyle: {
        fontSize: 11,
        lineHeight: 16,
        marginBottom: 20,
        marginTop:20,
        color: Colors.WHITE,
        fontFamily: 'Montserrat-Regular'
    },
    username: {
        fontSize: 11,
        lineHeight: 24,
        marginTop: 20,
        color: Colors.WHITE,
        textDecorationLine: 'underline',
        fontFamily: 'Montserrat-Bold'
    },
    copyrights: {
        height: 16,
        width: 16,
        marginRight: 5,
        resizeMode: 'contain'
    }
})