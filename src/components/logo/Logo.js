import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../style/Colors';
import BackButton from '../button/BackButton';

const {width} = Dimensions.get('window');

const Logo = (props) => {

    useEffect(() => {

    }, [])
    
    const { logoImg, backIcon } = props;

    return (
        <View style={styles.container} >

            {backIcon ? <BackButton /> : null}
            
            <Image
                source={logoImg}
                style={styles.logo}
            />
        </View>
    )
}

export default Logo;


const styles = StyleSheet.create({
    container: {
        padding: 10,
        paddingTop: 0,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        fontSize: 22,
        fontFamily: 'MysteryQuest-Regular'
    },
    logo: {
        height: 50, 
        width: 150,
        marginTop: 8,
    }
})