import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, TouchableOpacity, } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import Colors from '../../style/Colors';
import BarLine from '../bar/BarLine';

const {width} = Dimensions.get('window');

const CatBreeds = (props) => {

    const navigation = useNavigation();
    const [breedArr, setBreedArr] = useState([]);

    useEffect(() => {
        var data = props.data;

        console.log("cat breeed-----");

        const new_array = data.slice(0, 4);

        setBreedArr(new_array)
    }, [])

    const { title, subTitle } = props;

    return (
        <View style={styles.mainContainer} >

            <View>
                <Text style={styles.titleStyle} >{title}</Text> 
                <BarLine />
            </View>

            <View>
                <Text style={styles.subTitle} >{subTitle}</Text> 
            </View>

            <View>
                <View style={styles.bar} >
                    <Text></Text>
                </View>
                <FlatList
                    data={breedArr}
                    contentContainerStyle={styles.imageContainer}
                    renderItem={({item}) => (
                        <TouchableOpacity 
                            activeOpacity={0.6}
                            onPress={() => {navigation.navigate('Details', {details: item, referenceId: item.reference_image_id})}}
                            style={styles.image} >
                            <Image
                                source={{uri: item.image.url}}
                                style={styles.imageStyle}
                            />
                            <Text style={styles.catName} >{item.name}</Text>
                        </TouchableOpacity>
                    )}
                />
            </View>
           
        </View>
    )
}

export default CatBreeds;


const styles = StyleSheet.create({
    mainContainer: {
        width: width-40,
        margin: 20,
        marginTop: 0,
        padding: 20,
        paddingTop: 15,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
        backgroundColor: Colors.CREAM,
    },
    bar: {
        height: 100, 
        width: 30, 
        backgroundColor: Colors.YELLOW, 
        position: 'absolute', 
        top: 20,
        borderRadius: 20,
    },
    titleStyle: {
        fontSize: 16,
        lineHeight: 24,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    catName: {
        fontSize: 16,
        lineHeight: 24,
        marginTop: 10,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    },
    imageContainer: {
        flexWrap: 'wrap', 
        justifyContent: 'space-between', 
        flexDirection: 'row',
    },
    image: {
        margin: 10, 
        width: width/2-60, 
    },
    imageStyle: {
        height: 120, 
        width: 120, 
        borderRadius: 10,
    },
    subTitle: {
        fontSize: 20,
        lineHeight: 24,
        marginTop: 20,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    }
})
