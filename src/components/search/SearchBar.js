import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, TextInput, FlatList, TouchableOpacity } from 'react-native';
import { searchCatBreeds } from '../../api/api';

import { useNavigation } from '@react-navigation/native';

import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');


const SearchBar = (props) => {

    const navigation = useNavigation();

    const [focused, setFocused] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [breedArr, setBreedArr] = useState([]);

    const [errorText, setErrorText] = useState('');

    useEffect(() => {
        var data = props.data;

        console.log("cat breeed-----");

        setBreedArr(data)
    }, [])
    
    const { logoImg, title } = props;

    const searchCatsName = (text) => {

        if(searchText){
            setSearchText(text)

            searchCatBreeds(text)
            .then((resp) => {
                console.log(resp);
                setBreedArr(resp)

                console.log("=====---   " + resp.length);

                if(resp.length == 0){
                    setErrorText('No Suggestions Available!')
                }else{
                    setErrorText('')
                }
            })
            .catch((err) => {
                console.log(err);
            })
        }else{
            setBreedArr(props.data)
            setSearchText(text)
        }
       
    }

    const getDetails = (item) => {
        console.log('getDetails----');
        //console.log(item.image.url);

        navigation.navigate('Details', {
            details: item,
            referenceId: item.reference_image_id
        })
        setFocused(false); 
        setSearchText('')
    }

    return (
        <View style={styles.mainContainer} >

            <Image
                source={logoImg}
                style={styles.logo}
            />

            <Image
                source={{uri: 'https://s3-alpha-sig.figma.com/img/26d2/a232/4c7c4af04c6f8215244645b0d95c06e4?Expires=1655683200&Signature=ToARPFGoDBZZA48UdGfrfI86zqGAImhPzF09fRCL~HW-CpcnAXkRc5fO4xxOdK4luVoZ7tyRMqycu0uNv7H-E0t6dBql-qhJ3k0e4mEyjsG3PzdTwJ~fJia5DUTFESrzdeUlyA6TF0euX9dqlssEtJisMXafaA2-38Bc7ztCnoysNk7rRtz2jcXJLn5C-PzOHZsYQisjEkI4BF8wakaq-rmdXStHSFMYwMUopvDGy4Q-kB10qnYkeMtTUpI0K1hj3vW5nJvZ9mu7I0ZqQE~5GwWlFW3mGG-LA8u8Yu4A7axgDyepmLaQqqU3JhJYUxiIfouSJorMkK2UkXTmclYbpQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}}
                style={styles.logoBg}
            />

            <View style={styles.container} >
                <Text style={styles.titleStyle} >{title}</Text> 
            </View>

            <View style={[styles.searchBar, searchText ? {height: 250} : {}]}>
                <View  style={[ styles.searchContainer, focused ?  {width: '100%', justifyContent:'space-between'} : {width: '40%'} ]}>
                    <TextInput 
                        style={[styles.input, focused ? {width: width/2} : {width: 70}]}
                        placeholder='Search'
                        value={searchText}
                        onChangeText={(text) => {searchCatsName(text)}}
                        placeholderTextColor={Colors.LIGHT_BLACK}
                        onFocus={() => setFocused(true)} 
                    />
                    
                    {focused
                    ?
                    <TouchableOpacity
                        onPress={() => {setFocused(false); setSearchText('')}}
                    >
                        <Image
                            source={require('../../assets/cancel.png')}
                            style={styles.searchIcon}
                        />
                    </TouchableOpacity>
                    :
                     <TouchableOpacity
                        onPress={() => {setFocused(true); }}
                    >
                        <Image
                            source={require('../../assets/search.png')}
                            style={styles.searchIcon}
                        />
                    </TouchableOpacity>
                    
                    }
                </View>

                

                {searchText
                ?
                <View style={styles.searchContent} >
                    
                    {errorText.length !=0 ? 
                    <Text style={styles.errorText} >{errorText}</Text>
                    : 
                    <FlatList
                        nestedScrollEnabled
                        data={breedArr}
                        keyExtractor={(item) => item.id}
                        renderItem={({item}) => (
                            <TouchableOpacity 
                                onPress={() => {getDetails(item)}}
                                style={styles.itemContainer} >
                                <Text style={styles.itemText}>{item.name}</Text>
                            </TouchableOpacity>
                        )}
                    />   
                    }
                    
                 
                </View>
                :
                null}

              
            </View>

        </View>
    )
}

export default SearchBar;


const styles = StyleSheet.create({
    mainContainer: {
        width: width-40,
        margin: 20,
        //height: 200,
        marginBottom: 0,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: Colors.BLACK,
    },
    searchContainer: {
        flexDirection: 'row', 
        borderRadius: 20, 
        alignItems: 'center', 
        padding: 8, 
        height: 40,
        backgroundColor: 'white',
    },
    searchContent: {
        flex: 1, 
        marginTop: 10, 
        borderRadius: 15, 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    searchBar: { 
        margin: 20, 
        marginTop: 0,
    },
    searchIcon: {
        height: 18,
        width: 18,
        marginRight: 10,
        marginLeft: 5,
    },
    errorText: {
        fontSize: 18,
        fontFamily: 'Montserrat-Medium',
        color: Colors.LIGHT_BLACK
    },
    input: {
        height: 50, 
        fontSize: 14,
        paddingLeft: 10,
        fontFamily: 'Montserrat-Regular',
        color: Colors.LIGHT_BLACK
    },  
    itemText: {
        fontSize: 14,
        lineHeight: 24,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    itemContainer: {
        backgroundColor: 'rgba(151, 151, 151, 0.1)',
        padding: 10, 
        margin: 10, 
        width: width-100,
        borderRadius: 10, 
        marginBottom: 0
    },
    container: {
        padding: 10,
        width: 250, 
        padding: 20,
        paddingLeft: 20
    },
    text: {
        fontSize: 22,
        fontFamily: 'MysteryQuest-Regular'
    },
    logo: {
        height: 25, 
        width: 90,
        marginLeft: 20,
        marginTop: 20,
    },
    logoBg: {
        height: 200, 
        width: 150,
        position: 'absolute',
        right: 0,
        borderTopRightRadius: 30,
    },
    titleStyle: {
        fontSize: 16,
        lineHeight: 24,
        color: Colors.WHITE,
        fontFamily: 'Montserrat-Medium'
    }
})