import React, {useState, useEffect} from 'react'
import { Text, View, ActivityIndicator, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import Colors from '../../style/Colors'
import { useNavigation } from '@react-navigation/native';


const BackButton = () => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation.goBack()} style={styles.button} >
            <Image
                source={require('../../assets/back.png')}
                style={styles.icon}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        height: 36,
        width: 36,
        borderRadius: 10,
        borderWidth: 1.5,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 14,
        borderColor: Colors.LIGHT_BLACK,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        height: 16,
        width: 16
    }
})
export default BackButton