import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, ImageBackground, } from 'react-native';

import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const BarLine = (props) => {

    useEffect(() => {

    }, [])
    
    const {logoImg} = props;

    return (
        <View style={styles.mainContainer} >
             <View style={styles.barLine} />
        </View>
    )
}

export default BarLine;


const styles = StyleSheet.create({
    mainContainer: {},
    barLine: {
        height: 4,
        width: 60,
        marginTop: 6,
        borderRadius: 2,
        backgroundColor: '#4D270C'
    },
})