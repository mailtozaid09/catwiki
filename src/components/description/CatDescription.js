import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, TouchableOpacity, } from 'react-native';

import Colors from '../../style/Colors';
import BarLine from '../bar/BarLine';

const {width} = Dimensions.get('window');

const CatDescription = (props) => {

    useEffect(() => {

    }, [])
    
    const {readMore, title, subTitle} = props;

    return (
        <View style={styles.mainContainer} >
            <View style={styles.container} >
                <Text style={styles.titleStyle} >{title}</Text> 
                <Text style={styles.subTitle} >{subTitle}</Text> 

                <TouchableOpacity
                    onPress={readMore}
                    style={styles.readmoreContainer} >
                    <Text style={styles.readmore} >READ MORE</Text> 
                    <Image
                        source={require('../../assets/right-arrow.png')}
                        style={styles.rightArrow}
                    />
                </TouchableOpacity>
                
                <View style={{flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between'}} >
                    <View style={{marginRight: 5, alignItems: 'flex-end',}} >
                        <Image
                            source={require('../../assets/cat1.png')}
                            style={styles.catImg1}
                        />
                        <Image
                            source={require('../../assets/cat2.png')}
                            style={styles.catImg2}
                        />
                    </View>
                    <View style={{marginLeft: 5}} >
                        <Image
                            source={require('../../assets/cat3.png')}
                            style={styles.catImg3}
                        />
                    </View>
                </View>
            </View>
        </View>
    )
}

export default CatDescription;


const styles = StyleSheet.create({
    mainContainer: {
        width: width-40,
        margin: 20,
        marginTop: 0,
        marginBottom: 0,
    },
    container: {
        width: width-40,
    },
    readmoreContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    logo: {
        height: 30, 
        width: 120,
    },
    titleStyle: {
        fontSize: 30,
        lineHeight: 34,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Bold'
    },
    subTitle: {
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 20,
        color: Colors.LIGHT_BLACK,
        fontFamily: 'Montserrat-Medium'
    },
    readmore: {
        fontSize: 14,
        lineHeight: 20,
        marginTop: 25,
        marginBottom: 30,
        color: 'rgba(41, 21, 7, 0.6)',
        fontFamily: 'Montserrat-Bold'
    },
    rightArrow: {
        height: 24,
        width: 24,
        marginLeft: 10,
        marginBottom: 4,
        resizeMode: 'contain'
    },
    catImg1: {
        height: 120,
        resizeMode: 'contain',
        width: width/2 -20, 
        borderRadius: 20,
    },
    catImg2: {
        height: 170,
        resizeMode: 'contain',
        width: width/2 -60, 
        borderRadius: 20,
        marginTop: 10,
    },
    catImg3: {
        height: 240,
        resizeMode: 'contain',
        width: width/2 -20, 
        borderRadius: 20,
    }
})