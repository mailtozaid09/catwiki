const baseUrl = 'https://api.thecatapi.com/v1/'



// get cat breeds
export function getCatBreeds() {
    return(
        fetch(
            baseUrl+'breeds',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}


// search cat breeds
export function searchCatBreeds(query) {
    return(
        fetch(
            baseUrl+`breeds/search?q=${query}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// get reference image
export function getReferenceImg(id) {
    return(
        fetch(
            baseUrl+`images/${id}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

